# Tutorial_Subir_un Paquete_a_Gitlab

## Name
Tutorial para subir un paquete a Gitlab desde la terminal.

## Justification
La razón por la cual me decidí a realizar este proyecto en específico es porque durante la clase de Linux en la que aprendía este mismo tema la persona que lo explicaba se enredó un poco y tardó más de lo esperado en hacerlo, entonces me dí cuenta que la verdad si es un poco tedioso hacerlo la primera vez o hacerlo después de no haber hecho algo relacionado durante una cantidad de tiempo considerable, además, para alguien que recién está aprendiendo a usar la terminal es aún más difícil, por lo que pensé que sería un proyecto muy útil.

## Description
El proyecto incluye un video, en el cual se explica como subir un arcivo a la página web de Gitlab desde la terminal de linux. Incluye dos formas de hacerlo, clonando el repositorio de Gitlab al equipo o subiendolo directamente desde el almacenamiento local. La idea es facilitar dicho proceso a personas nuevas en este sistema o personas que no lo utilizan con regularidad, lo cual podria ser un poco tedioso.

## Objectives
Objetivo General:Desarrollar un tipo de material mediante el cual las personas puedan aprender a subir un archivo desde la terminal de Linux a un proyecto en la plataforma de GitLab. 

Objetivos Específicos Usar otro tipo de herramientas de software libre que ofrece Linux para grabar y editar el tutorial propuesto, Buscar la licencia de uso que mejor se adapre al proyecto, Hacer un material de la mejor calidad posible y fácil de entender para nuevos usuarios en este tipo de Sistema. 

## Installation
Debido a que el video tiene un peso original alrededor de 1.6 gb, fue necesario comprimirlo en un archivo .rar por lo que es necesario, en caso de querer verlo, un software de descompresion dearchivos (la mejor idiea seria usar 7-zip debido a que es igualmente libre, sin embargo hay mas alternativas)

## Authors and acknowledgment
Carlos Sneider Fandiño Hernández

## License
La licencia utilizada es la MIT license debido a que es una licencia permisiva breve y simple con condiciones que solo requieren la preservación de los derechos de autor y avisos de licencia.
